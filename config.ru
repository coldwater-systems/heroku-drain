# frozen_string_literal: true

require 'bundler/setup'
require 'rack'
require_relative 'lib/heroku_drain'

run HerokuDrain::API
