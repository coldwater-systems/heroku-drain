**NOTICE:** This project is no longer maintained. Email <me@a.lexg.dev> for maintainership inquiries.

# Heroku Drain

A bare bones Logplex receiver designed to run as a systemd user service.

See Heroku's [Log Drains](https://devcenter.heroku.com/articles/log-drains#https-drains) for details.

## Setup

This project depends on Ruby 3.

- After cloning, run the following commands:

  ```bash
  gem install bundler # if necessary
  bundle install
  rake systemd:setup
  ```

- To start the service, run `systemctl --user enable --now heroku-drain@3000.service`, where 3000 can be replaced by any other port number.
- You probably also want to run `sudo loginctl enable-linger $USER` so that your user services do not stop when you log out.
- Finally, you will need to reverse-proxy to the application server with a web-server like nginx or apache.
