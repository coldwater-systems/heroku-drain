# frozen_string_literal: true


module HerokuDrain
	class Error < StandardError; end
end

require_relative 'heroku_drain/api'
