# frozen_string_literal: true

require 'grape'

class HerokuDrain::API < Grape::API
	content_type :logplex, 'application/logplex-1'
	format :logplex

	resource :board do
		post :logs do
			puts env['rack.input'].read

			# No response
			status 204
			''
		end
	end
end
